## Setup on CentOS 7

#### Install Java
The frontend code uses `cytoscape` and `cytoscape` needs Java. If Java isn't installed then `grunt deps` command would exit without completing.
Use the following script to install Oracle Java

```
#!/usr/bin/env bash

# A fresh CentOS 7 would require wget

yum install -y wget

wget https://storage.googleapis.com/tempneo/jdk-8u221-linux-x64.tar.gz

# extract from the archive
tar -xzf jdk-8u221-linux-x64.tar.gz -C /opt

ln -s /opt/jdk1.8.0_181 /opt/jdk
```

Copy the above script into a file. Let's say you name the file `install-java.sh`. Make the file executable and install as follows:
```
chmod +x install-java.sh
./install-java.sh

export JAVA_HOME=/opt/jdk
export PATH=$PATH:/opt/jdk/bin  
```

#### Install Node version 4
The following steps will install `node` version 8 and then use it to install version 4 (more specifically 4.9.1)
```
yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup_8.x | sudo -E bash -
yum install nodejs
npm install -g n
n 4.9.1
reboot
```
Check the node version with the `node -v` command. Verify that the output should be `4.9.1`

#### Install NeoPages
```
yum install -y git
git clone https://aisenberg@bitbucket.org/aisenberg/neopages.git
```

## Setup

Install node build dependencies
```
# After cloning the repository, enter the directory and install node dependencies

npm install

# Switch to public directory and install global dependencies. Might need to be root for the install steps
cd public
npm install -g grunt bower inherits
npm install -g grunt-cli

# Install the rest of the dev dependencies from package.json file
npm install

# Install JavaScript browser dependencies
grunt deps

# Compile less directory
grunt less:development

# Compile js directory
grunt requirejs:development

# Launch the app `(127.0.0.1:3000)`
cd ../ && node app.js
```
### Note

If the exec task fails, download libs.tar.gz from `https://drive.google.com/open?id=1hICb9EjjAGq3QtbouehHbiNXk7UcTGsO` and replace with the libs folder in public directory.
