####Weekly
- [ ]45. Track the impact of your work or value being created
1. How would you quantify the impact your last week's work might have had.
2. How would you forecast the impact of your work in the future?
In a way you could put a quantity along a timeline that might forecast the future impact of your current actions, then this weekly activity would become that of checking if the plan went as expected or whether it needs changes.
