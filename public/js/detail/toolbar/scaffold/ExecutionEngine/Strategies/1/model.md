Although the desired state looks stupidly idealist, it would still make an exciting pursuit. It seems unrealistic because in my head the search space seems too vast and intractable. I'm sure we can explore ways of breaking down the problem into smaller ones. For instance:

1. Maintain a database of known execution ExecutionEngines
2. Setup a mechanism to keep updating the database with new execution ExecutionEngines
3. Setup a mechanism to keep updating the database with updates to the execution engines already added.

This is so much like the package manager of some operating system. The difference simply being that the OS searches through programs while we are looking to search through the entire world.

The most challenging aspect would be discovery - step 2 from above. Discovery itself could be divided into two parts:
1. Searching for execution engines already available elsewhere
2. Creating new execution engines and then assimilating them to the database.

Another way to break down the problem could be to go about the above mentioned tasks separately for each options - myself, people, tech of natural processes. For instance, the tech infrastructure section could take care of maintaining this database for only the tech part.
