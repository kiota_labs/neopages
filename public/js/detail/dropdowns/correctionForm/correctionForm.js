define([
    'flight/lib/component',
    '../withDropdown',
    'tpl!./correctionForm',
    'tpl!util/alert',
    'util/vertex/formatters',
    'util/withDataRequest',
    'keyboard',
], function(
    defineComponent,
    withDropdown,
    correctionTemplate,
    alertTemplate,
    F,
    withDataRequest,
    keyboard) {
    'use strict';

    return defineComponent(CorrectionForm, withDropdown, withDataRequest);

    function CorrectionForm() {

        this.defaultAttrs({
            inputSelector: 'textarea',
            primarySelector: '.btn-primary'
        });

        this.after('initialize', function() {
            var self = this;

            this.on('change keyup paste', {
                inputSelector: this.onChange
            });
            this.on('click', {
                primarySelector: this.onSave
            });
            this.on('visibilitychange', this.onVisibilityChange);

            this.$node.html(correctionTemplate({
                graphVertexId: this.attr.data.id,
                correctionText: this.attr.correction && this.attr.correction.value || '',
                buttonText: i18n('detail.correction.form.button')
            }));

            this.on('opened', function() {
                //this.select('inputSelector').keyboard().getkeyboard()
                this.select('inputSelector').keyboard({layout:'zh'})
                console.log(keyboard)
            });

            require([
                'configuration/plugins/visibility/visibilityEditor'
            ], function(Visibility) {
                Visibility.attachTo(self.$node.find('.visibility'), {
                    value: self.attr.comment &&
                        self.attr.comment.metadata &&
                        self.attr.comment.metadata['http://neo.io#visibilityJson'] &&
                        self.attr.comment.metadata['http://neo.io#visibilityJson'].source
                });
            });

            this.checkValid();
        });

        this.onChange = function(event) {
            this.checkValid();
        };

        this.onVisibilityChange = function(event, data) {
            this.visibilitySource = data;
            this.checkValid();
        };


        this.onSave = function(event) {
            var self = this,
                $mentionNode = $(this.attr.mentionNode),
                newObjectSign = $(this.attr.sign),
                range = this.attr.selection.range,
                mentionStart,
                mentionEnd;

              var newRange = document.createRange();
              newRange.setStart(range.startContainer, range.startOffset);
              newRange.setEnd(range.endContainer, range.endOffset);

              var r = range.cloneRange();
              r.selectNodeContents(this.$node.closest('.text').get(0));

              r.setEnd(range.startContainer, range.startOffset);
              var l = r.toString().length;
              this.selectedStart = l;
              this.selectedEnd = l + range.toString().length;

            if (this.attr.existing) {
                var dataInfo = $mentionNode.data('info');
                mentionStart = dataInfo.start;
                mentionEnd = dataInfo.end;
            } else {
                mentionStart = this.selectedStart;
                mentionEnd = this.selectedEnd;
            }


            this.buttonLoading();
            var parameters = {
                sign: newObjectSign,
                incorrect: newObjectSign,
                propertyKey: this.attr.propertyKey,
                conceptId: this.selectedConceptId,
                mentionStart: mentionStart,
                mentionEnd: mentionEnd,
                artifactId: this.attr.artifactId,
                value: this.getValue(),
                correct: this.getValue(),
                visibilitySource: this.visibilitySource || ''
            };

            if (newObjectSign.length) {
                parameters.objectSign = newObjectSign;
                $mentionNode.attr('title', newObjectSign);
            }

            if (self.attr.snippet) {
                parameters.sourceInfo = {
                    vertexId: parameters.artifactId,
                    textPropertyKey: parameters.propertyKey,
                    startOffset: parameters.mentionStart,
                    endOffset: parameters.mentionEnd,
                    snippet: self.attr.snippet
                };
            }

            this.dataRequest('vertex', 'correctText', parameters)
                .then(function(data) {
                    self.highlightTerm(data);
                    _.defer(self.teardown.bind(self));
                })
                .catch(this.requestFailure.bind(this))
        };

        this.getValue = function() {
            return $.trim(this.select('inputSelector').val());
        };

        this.requestFailure = function(request, message, error) {
            this.markFieldErrors(error);
            _.defer(this.clearLoading.bind(this));
        };

        this.checkValid = function() {
            var val = this.getValue();

            if (val.length && this.visibilitySource && this.visibilitySource.valid) {
                this.select('primarySelector').removeAttr('disabled');
            } else {
                this.select('primarySelector').attr('disabled', true);
            }
        }
    }
});
