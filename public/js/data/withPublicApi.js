define([], function() {
    'use strict';

    return withPublicApi;

    function withPublicApi() {

        this.before('initialize', function() {
            this.neoData = {};
            window.neoData = this.neoData;
        });

        this.setPublicApi = function(key, obj, options) {
            options = _.extend({
                    onlyIfNull: false
                }, options || {});

            if (options.onlyIfNull && (key in this.neoData)) {
                return;
            }

            if (typeof obj === 'undefined') {
                delete this.neoData[key];
            } else {
                this.neoData[key] = obj;
                this.trigger(key + 'NeoDataUpdated', {
                    key: key,
                    object: obj
                });
            }

            this.worker.postMessage({
                type: 'publicApi',
                key: key,
                obj: obj
            });
        }
    }
});
