define([], function() {
    'use strict';
    return function(message) {
        if (typeof atmosphere !== 'undefined') {
            // TODO: add sourceId to filter current users requests
            //Subscribe or connect to the remote server. The request's transport(web sockets) will be used.
            publicData.socket = atmosphere.subscribe(_.extend(message.configuration, {
                //Invoked when the connection gets opened
                onOpen: function(response) {
                    if (atmosphere.util.__socketPromiseFulfill) {
                        atmosphere.util.__socketPromiseFulfill(publicData.socket);
                    } {
                        atmosphere.util.__socketOpened = true;
                    }
                },
                //Invoked when an unexpected error occurs
                onError: function(request) {
                    dispatchMain('websocketStateOnError', {
                        reason: request.reasonPhrase,
                        error: request.error
                    });
                },
                //Invoked when the connection gets closed
                onClose: function(request) {
                    atmosphere.util.__socketOpened = false;
                    dispatchMain('websocketStateOnClose', {
                        reason: request.reasonPhrase,
                        error: request.error
                    });
                },
                //Invoked when a message gets delivered
                onMessage: function(response) {
                    processMainMessage({
                        type: 'websocketMessage',
                        responseBody: response.responseBody
                    });
                }
            }));
        }
    };
})
