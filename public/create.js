var concepts = [ 'http://neo.io#process',
'http://neo.io#publishedDate',
'http://neo.io#raw',
'http://neo.io#rawPosterFrame',
'http://neo.io#rowKey',
'http://neo.io#source',
'http://neo.io#sourceUrl',
'http://neo.io#text',
'http://neo.io#video-mp4',
'http://neo.io#video-webm',
'http://neo.io#videoFrame',
'http://neo.io#videoPreviewImage',
'http://neo.io#visibilityJson',
'http://neo.io#visibilitySource',
'http://neo.io/comment#entry',
'http://neo.io/workspace#access',
'http://neo.io/workspace#creator',
'http://neo.io/workspace#graphPositionX',
'http://neo.io/workspace#graphPositionY',
'http://neo.io/workspace#visible' ];

var code = "";
for (var i = 0, len = concepts.length; i < len; i++) {
    code = code+" case " + "\"" + concepts[i] + "\"" + ": break; \n";
}

console.log(code);