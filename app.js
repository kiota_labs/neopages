var express = require("express"),
    path    = require("path"),
    hbs     = require("express-handlebars");

var app = express();

const configData       = require(__dirname+ "/public/api/config.json"),
      userData         = require(__dirname+ "/public/api/user.json"),
      ontologyData     = require(__dirname+ "/public/api/ontology.json")

app.engine('hbs', hbs({extname: 'hbs'}));

app.set("view engine", "hbs");

app.use(express.static(__dirname + "/public"));

app.get("/", function(req, res){
  res.render(path.join(__dirname, "/public/index"), {
    title: "neoWeb",
    description: "NEO Frontend"
  });
});

app.get("/configuration", function(req, res){
  res.json(configData);
});

app.get("/user/me", function(req, res){
  res.json(userData);
});

app.get("/ontology", function(req, res){
  res.json(ontologyData);
});


app.get("/notification/all", function(req, res){
  res.json(
    {"system":{"future":[],"active":[]},"user":[]}
  );
});

app.get("/workspace/diff", function(req, res){
  res.json(diffData);
});

app.get("/jsc/data/web-worker/messaging", function(req, res){
});


app.get("/resource", function(req, res){
  res.sendFile(__dirname+ "/public/img/glyphicons/glyphicons_036_file@2x.png");
});

app.get("/vertex/thumbnail", function(req, res){
  res.sendFile(__dirname+ "/public/img/glyphicons/glyphicons_011_camera@2x.png")
})

app.listen(3000, '0.0.0.0', function(){
  console.log("Server has started!");
});
