# Neo on Express
=======
# Neo Express

Frontend Code for NEO served by Express on Nodejs

# Installation Summary
This README includes steps to setup the frontend environment for the following environments:

1. Windows
2. Mac OSX
3. CentOS 7

## Windows Setup

### Setup on Virtual Box
1. Download and install virtualbox
2. Download the CentOS 7 minimal install ISO
3. Create a new virtual machine running CentOS 7
4. Configure the virtual machine as required
5. Install Oracle Java
6. Install Frontend dependencies
7. Build the app
8. Run the server

#### Virtualbox
Follow this [tutorial](https://www.wikihow.com/Install-VirtualBox) for Windows.

#### Download CentOS 7 ISO
A lot of mirrors are listed on [this location](http://isoredirect.centos.org/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1804.iso) for the ISO file. Use any link that works. They all link to the same file. Remember the folder on your machine where you download and store this file.

#### Create a CentOS VM
1. Start VirtualBox
2. Click on the ‘New’ icon in the toolbar or press Ctrl+N to create a new virtual machine. A dialog box should appear
3. **Name**: Choose a name for your new Virtual Machine. Let's say you name it *VadaPao*  
  **Type**: Linux  
  **Version**: Red Hat (32bit/64bit)
4. Set your VM Memory to at least 2048MB(2 GB) before you continue.
5. Click Next
6. Select ‘Create a virtual hard drive now’ and then ‘Create’.
7. Select ‘VDI(VirtualBox Disk Image) and ‘Next’.
8. Select ‘Dynamically allocated’ and then ‘Next’.
9. Leave the default file name in the name box or rewrite it as your choice.
10. For the size of the virtual hard drive enter 20GB and then ‘Create’.
11. Right click on the ‘VadaPao’ and select ‘Settings’.
12. In the **Storage** tab, go to **Controller: IDE**. Click on the button to the right of the Optical Drive field and select **Choose Virtual Optical Disk File**
13. Browse to the location of the *.ISO* file and select it.
14. In the **Network** tab, check the *Enable Network Adapter* box
15. **Attached To**: Select *Bridged Adapter*
16. Now power on the virtual machine by selecting ‘Start’ and then ‘Normal Start’

#### Complete the OS installation
1. Select the ‘Install CentOS 7’ option and the process will begin soon.
2. Select your language and press ‘Continue’.
3. Select ‘DATE & TIME’ and choose your time zone.
4. Just check that the **Software Selection** tab should read **Minimal Install**
5. Next, select **INSTALLATION DESTINATION** under **SYSTEM** category and select the 20GB ATA VBOX Harddisk. Then select **Done**
6. Set the **NETWORK & HOSTNAME** and turn on the “Ethernet switch” and give a hostname.
7. Next, press **Begin Installation**
8. While VM is installing, set **ROOT PASSWORD** and create another user with the help of **USER CREATION** option.
9. Wait for the installation process, it can take some time
10. After installation of your CentOS 7 on your VM, press “Reboot”.  
After rebooting it will automatic boot up and take you to the Login screen. Login using `root` and the password you had set.

---

## Setup on Mac OSX
Make sure Java and node are installed. Then follow the same steps as mentioned for [CentOS setup](centOS.md)
